#!/bin/bash

mkfs.btrfs -d single -m raid1 /dev/nvme0n1 /dev/nvme1n1

mkdir --parents /mnt/gentoo
mount /dev/nvme1n1 /mnt/gentoo

cd /mnt/gentoo

if [ ! -f stage3-amd64-* ]; then
    wget https://bouncer.gentoo.org/fetch/root/all/releases/amd64/autobuilds/$(curl -sL https://bouncer.gentoo.org/fetch/root/all/releases/amd64/autobuilds/latest-stage3-amd64-systemd.txt | tail -1 | awk '{print $1}')
fi

